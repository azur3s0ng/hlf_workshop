package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//IoTChainCode struct
type IoTChainCode struct {
}

//Transaction struct
type Transaction struct {
	TransactionID string `json:"transaction_id"`
	Timestamp     string `json:"timestamp"`
	HashedMSG     string `json:"hashed_message"`
	Status        string `json:"status"`
}

//TransactionFirmware struct
type TransactionFirmware struct {
	TransactionID  string `json:"transaction_id"`
	Timestamp      string `json:"timestamp"`
	HashedFirmware string `json:"hashed_firmware"`
	DeviceType     string `json:"device_type"`
	Version        string `json:"version"`
	Received       int64  `json:"received"`
	Status         string `json:"status"`
	FailedAttemps  int    `josn:"failed_attemps"`
}

//VerifyResponse struct
type VerifyResponse struct {
	Code  int                 `json:"code"`
	Asset TransactionFirmware `json:"asset"`
}

//FinalVerifyResponse struct
type FinalVerifyResponse struct {
	Status        string `json:"status"`
	FailedAttemps int    `json:"failed_attemps"`
}

//Init function
func (t *IoTChainCode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	//_, args := stub.GetFunctionAndParameters()
	var err error

	genesisTransaction := Transaction{
		TransactionID: "iot-genesis",
		Timestamp:     "2018-6-2 20:41:00",
		HashedMSG:     "cc1f733411beea64b98e5fb30eb877240dd93dcb19c11bda91b04cbc93d99199",
		Status:        "verified",
	}

	transactionBytes, err := json.Marshal(genesisTransaction)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on marshaling gensis block."))
	}

	err = stub.PutState(genesisTransaction.TransactionID, transactionBytes)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on creating the gensis block."))
	}

	return shim.Success([]byte(string(transactionBytes[:])))
}

//Invoke function
func (t *IoTChainCode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()

	if function == "send" {
		return t.send(stub, args)
	} else if function == "validate" {
		return t.validate(stub, args)
	} else if function == "push" {
		return t.push(stub, args)
	} else if function == "verify" {
		respBytes := t.verify(stub, args).Payload

		var resp VerifyResponse
		err := json.Unmarshal([]byte(respBytes), &resp)
		if err != nil {
			return shim.Error(fmt.Sprintf("Error unmarshal transaction."))
		}

		switch code := resp.Code; code {
		case 0:
			return shim.Error(fmt.Sprintf("ERROR!! too many failed attemps, verification rejected."))
		case 1:
			resp.Asset.FailedAttemps++
			return t.update(stub, resp.Asset)
			//return shim.Error(fmt.Sprintf("ERROR!!! verification failed, hash does not match."))
		case 2:
			return shim.Error(fmt.Sprintf("ERROR!!! transaction is expired."))
		case 99:
			resp.Asset.Status = "verified"
			return t.update(stub, resp.Asset)
		default:
			return shim.Error(fmt.Sprintf("ERROR!!! error occured at verify process."))
		}
	} else if function == "query_status" {
		return t.queryStatus(stub, args)
	}
	return shim.Error("Invalid invoke function name. Expecting \"send \" \"validate\" \"push\" \"verify\"")
}

func (t *IoTChainCode) send(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var err error

	if len(args) != 3 {
		return shim.Error(fmt.Sprintf("Error on receiving parameters."))
	}

	transactionID := args[0]
	timestamp := args[1]
	hashedMSG := args[2]

	sendTransaction := Transaction{
		TransactionID: transactionID,
		Timestamp:     timestamp,
		HashedMSG:     hashedMSG,
		Status:        "unverified",
	}

	transactionBytes, err := json.Marshal(sendTransaction)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on marshaling new block."))
	}

	err = stub.PutState(sendTransaction.TransactionID, transactionBytes)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on creating new block."))
	}

	return shim.Success([]byte(string(transactionBytes[:])))
}

func (t *IoTChainCode) validate(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var err error

	if len(args) != 2 {
		return shim.Error(fmt.Sprintf("Error on receiving parameters."))
	}

	transactionID := args[0]
	hashedMSG := args[1]

	transactionBytes, err := stub.GetState(transactionID)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on retrieving state " + transactionID + "."))
	}

	var transaction Transaction
	err = json.Unmarshal([]byte(transactionBytes), &transaction)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error unmarshal transaction."))
	}

	if hashedMSG == transaction.HashedMSG {
		transaction.Status = "verified"
		return shim.Success([]byte("transaction is valid."))
	}

	return shim.Error(fmt.Sprintf("ERROR!!! transaction is invalid."))
}

func (t *IoTChainCode) push(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var err error

	if len(args) != 5 {
		return shim.Error(fmt.Sprintf("Error on receiving parameters."))
	}

	transactionID := args[0]
	timestamp := args[1]
	hashedFirmware := args[2]
	deviceType := args[3]
	version := args[4]

	pushTransaction := TransactionFirmware{
		TransactionID:  transactionID,
		Timestamp:      timestamp,
		HashedFirmware: hashedFirmware,
		DeviceType:     deviceType,
		Version:        version,
		Received:       time.Now().Unix(),
		Status:         "unverified",
		FailedAttemps:  0,
	}

	transactionBytes, err := json.Marshal(pushTransaction)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on marshaling new block."))
	}

	err = stub.PutState(pushTransaction.TransactionID, transactionBytes)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on creating new block."))
	}

	return shim.Success([]byte(string(transactionBytes[:])))
}

func (t *IoTChainCode) verify(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var err error

	if len(args) != 2 {
		return shim.Error(fmt.Sprintf("Error on receiving parameters."))
	}

	transactionID := args[0]
	hashedFirmware := args[1]
	//deviceType := args[2]
	//version := args[3]

	transactionBytes, err := stub.GetState(transactionID)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on retrieving state " + transactionID + "."))
	}

	var transaction TransactionFirmware
	err = json.Unmarshal([]byte(transactionBytes), &transaction)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error unmarshal transaction."))
	}

	currentTime := time.Now().Unix()

	if currentTime-transaction.Received < 60 {
		if transaction.FailedAttemps > 3 {
			resp := VerifyResponse{
				Code:  0,
				Asset: transaction,
			}
			respBytes, err := json.Marshal(resp)
			if err != nil {
				return shim.Error(fmt.Sprintf("Error on marshaling object"))
			}
			return shim.Success([]byte(string(respBytes[:])))
			//return shim.Error(fmt.Sprintf("ERROR!! too many failed attemps, verification rejected."))
		}
		if hashedFirmware == transaction.HashedFirmware {
			resp := VerifyResponse{
				Code:  99,
				Asset: transaction,
			}
			respBytes, err := json.Marshal(resp)
			if err != nil {
				return shim.Error(fmt.Sprintf("Error on marshaling object"))
			}
			return shim.Success([]byte(string(respBytes[:])))
			//transaction.Status = "verified"
			//return shim.Success([]byte("firmware update is valid."))
		}
	} else {
		resp := VerifyResponse{
			Code:  2,
			Asset: transaction,
		}
		respBytes, err := json.Marshal(resp)
		if err != nil {
			return shim.Error(fmt.Sprintf("Error on marshaling object"))
		}
		return shim.Success([]byte(string(respBytes[:])))
		//return shim.Error(fmt.Sprintf("ERROR!!! transaction is expired."))
	}

	resp := VerifyResponse{
		Code:  1,
		Asset: transaction,
	}

	respBytes, err := json.Marshal(resp)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on marshaling object"))
	}

	return shim.Success([]byte(string(respBytes[:])))
}

func (t *IoTChainCode) update(stub shim.ChaincodeStubInterface, asset TransactionFirmware) pb.Response {
	var err error

	transactionBytes, err := json.Marshal(asset)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on marshaling new block."))
	}

	err = stub.PutState(asset.TransactionID, transactionBytes)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on creating new block."))
	}

	return shim.Success([]byte(string(transactionBytes[:])))
}

func (t *IoTChainCode) queryStatus(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var err error

	if len(args) != 1 {
		return shim.Error(fmt.Sprintf("Error on receiving parameters."))
	}

	transactionID := args[0]

	transactionBytes, err := stub.GetState(transactionID)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on retrieving state " + transactionID + "."))
	}

	var transaction TransactionFirmware
	err = json.Unmarshal([]byte(transactionBytes), &transaction)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error unmarshal transaction."))
	}

	resp := FinalVerifyResponse{
		Status:        transaction.Status,
		FailedAttemps: transaction.FailedAttemps,
	}

	respBytes, err := json.Marshal(resp)
	if err != nil {
		return shim.Error(fmt.Sprintf("Error on marshaling object"))
	}

	return shim.Success([]byte(string(respBytes)))
}

func main() {
	err := shim.Start(new(IoTChainCode))
	if err != nil {
		fmt.Printf("Error starting IoT chaincode: %s", err)
	}
}
