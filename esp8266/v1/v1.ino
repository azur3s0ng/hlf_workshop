/*
  To upload through terminal you can use: curl -F "image=@firmware.bin" esp8266-webupdate.local/update
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "src/ESP8266HTTPUpdateServer_mod.h"

const char* host = "esp8266-webupdate";
const char* ssid = "SEAT";
const char* password = "seat2017";

ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater(true);

void setup(void){

  Serial.begin(115200);
  Serial.println();
  Serial.println("Booting Sketch...");
  WiFi.mode(WIFI_AP_STA);
  WiFi.begin(ssid, password);

  while(WiFi.waitForConnectResult() != WL_CONNECTED){
    WiFi.begin(ssid, password);
    Serial.println("WiFi failed, retrying.");
  }

  MDNS.begin(host);

  httpUpdater.setup(&httpServer);
  httpServer.begin();

  MDNS.addService("http", "tcp", 80);
  Serial.printf("HTTPUpdateServer ready! Open http://%s/update in your browser\n", WiFi.localIP().toString().c_str());
  Serial.printf("firmware v1.0\n\n");
}

void loop(void){
  httpServer.handleClient();
}
