#!/bin/bash

echo
echo " ____    _____      _      ____    _____ "
echo "/ ___|  |_   _|    / \    |  _ \  |_   _|"
echo "\___ \    | |     / _ \   | |_) |   | |  "
echo " ___) |   | |    / ___ \  |  _ <    | |  "
echo "|____/    |_|   /_/   \_\ |_| \_\   |_|  "
echo
echo "Build your first network (BYFN) end-to-end test"
echo
CHANNEL_NAME="$1"
DELAY="$2"
: ${CHANNEL_NAME:="mychannel"}
: ${TIMEOUT:="10"}
COUNTER=1
MAX_RETRY=5
ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

echo "Channel name : "$CHANNEL_NAME

# verify the result of the end-to-end test
verifyResult () {
	if [ $1 -ne 0 ] ; then
		echo "!!!!!!!!!!!!!!! "$2" !!!!!!!!!!!!!!!!"
    echo "========= ERROR !!! FAILED to execute End-2-End Scenario ==========="
		echo
   		exit 1
	fi
}

setGlobals () {

	if [ $1 -eq 0 -o $1 -eq 1 ] ; then
		CORE_PEER_LOCALMSPID="Org1MSP"
		CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
		CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
		if [ $1 -eq 0 ]; then
			CORE_PEER_ADDRESS=peer0.org1.example.com:7051
		else
			CORE_PEER_ADDRESS=peer1.org1.example.com:7051
			CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
		fi
	elif [ $1 -eq 2 -o $1 -eq 3 ] ; then
		CORE_PEER_LOCALMSPID="Org2MSP"
		CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
		CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
		if [ $1 -eq 2 ]; then
			CORE_PEER_ADDRESS=peer0.org2.example.com:7051
		else
			CORE_PEER_ADDRESS=peer1.org2.example.com:7051
		fi
	elif [ $1 -eq 4 -o $1 -eq 5 ] ; then
		CORE_PEER_LOCALMSPID="Org3MSP"
		CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org3.example.com/peers/peer0.org3.example.com/tls/ca.crt
		CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org3.example.com/users/Admin@org3.example.com/msp
		if [ $1 -eq 4 ]; then
			CORE_PEER_ADDRESS=peer0.org3.example.com:7051
		else
			CORE_PEER_ADDRESS=peer1.org3.example.com:7051
		fi
	else
		CORE_PEER_LOCALMSPID="Org4MSP"
		CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org4.example.com/peers/peer0.org4.example.com/tls/ca.crt
		CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org4.example.com/users/Admin@org4.example.com/msp
		if [ $1 -eq 6 ]; then
			CORE_PEER_ADDRESS=peer0.org4.example.com:7051
		else
			CORE_PEER_ADDRESS=peer1.org4.example.com:7051
		fi
	fi

	env |grep CORE
}

createChannel() {
	setGlobals 0

  if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
		peer channel create -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx  >&log.txt
	else
		peer channel create -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
	fi
	res=$?
	cat log.txt
	verifyResult $res "Channel creation failed"
	echo "===================== Channel \"$CHANNEL_NAME\" is created successfully ===================== "
	echo
}

updateAnchorPeers() {
  PEER=$1
  setGlobals $PEER

  if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
		peer channel update -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx >&log.txt
	else
		peer channel update -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
	fi
	res=$?
	cat log.txt
	verifyResult $res "Anchor peer update failed"
	echo "===================== Anchor peers for org \"$CORE_PEER_LOCALMSPID\" on \"$CHANNEL_NAME\" is updated successfully ===================== "
	sleep $DELAY
	echo
}

## Sometimes Join takes time hence RETRY atleast for 5 times
joinWithRetry () {
	peer channel join -b $CHANNEL_NAME.block  >&log.txt
	res=$?
	cat log.txt
	if [ $res -ne 0 -a $COUNTER -lt $MAX_RETRY ]; then
		COUNTER=` expr $COUNTER + 1`
		echo "PEER$1 failed to join the channel, Retry after 2 seconds"
		sleep $DELAY
		joinWithRetry $1
	else
		COUNTER=1
	fi
  verifyResult $res "After $MAX_RETRY attempts, PEER$ch has failed to Join the Channel"
}

joinChannel () {
	for ch in 0 1 2 3 4 5 6 7; do
		setGlobals $ch
		joinWithRetry $ch
		echo "===================== PEER$ch joined on the channel \"$CHANNEL_NAME\" ===================== "
		sleep $DELAY
		echo
	done
}

installChaincode () {
	PEER=$1
	setGlobals $PEER
	peer chaincode install -n mycc -v 1.0 -p github.com/hyperledger/fabric/examples/chaincode/go/iot >&log.txt
	res=$?
	cat log.txt
        verifyResult $res "Chaincode installation on remote peer PEER$PEER has Failed"
	echo "===================== Chaincode is installed on remote peer PEER$PEER ===================== "
	echo
}

instantiateChaincode () {
	PEER=$1
	setGlobals $PEER
	# while 'peer chaincode' command can get the orderer endpoint from the peer (if join was successful),
	# lets supply it directly as we know it using the "-o" option
	if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
		peer chaincode instantiate -o orderer.example.com:7050 -C $CHANNEL_NAME -n mycc -v 1.0 -c '{"Args":["init"]}' -P "OR	('Org1MSP.member','Org2MSP.member','Org3MSP.member','Org4MSP.member')" >&log.txt
	else
		peer chaincode instantiate -o orderer.example.com:7050 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME -n mycc -v 1.0 -c '{"Args":["init"]}' -P "OR	('Org1MSP.member','Org2MSP.member','Org3MSP.member','Org4MSP.member')" >&log.txt
	fi
	res=$?
	cat log.txt
	verifyResult $res "Chaincode instantiation on PEER$PEER on channel '$CHANNEL_NAME' failed"
	echo "===================== Chaincode Instantiation on PEER$PEER on channel '$CHANNEL_NAME' is successful ===================== "
	echo
}

chaincodeQuery () {
  PEER=$1
  echo "===================== Querying on PEER$PEER on channel '$CHANNEL_NAME'... ===================== "
  setGlobals $PEER
  local rc=1
  local starttime=$(date +%s)

  # continue to poll
  # we either get a successful response, or reach TIMEOUT
  while test "$(($(date +%s)-starttime))" -lt "$TIMEOUT" -a $rc -ne 0
  do
     sleep $DELAY
  	 echo "Attempting to Query PEER$PEER ...$(($(date +%s)-starttime)) secs"
  	 peer chaincode query -C $CHANNEL_NAME -n mycc -c '{"Args":["query"]}' >&log.txt
#     test $? -eq 0 && VALUE=$(cat log.txt | awk '/Query Result/ {print $NF}')
#     test "$VALUE" = "$2" && let rc=0
  done
  echo
  cat log.txt

#  if test $rc -eq 0 ; then
#	echo "===================== Query on PEER$PEER on channel '$CHANNEL_NAME' is successful ===================== "
#  else
#	echo "!!!!!!!!!!!!!!! Query result on PEER$PEER is INVALID !!!!!!!!!!!!!!!!"
#        echo "================== ERROR !!! FAILED to execute End-2-End Scenario =================="
#	echo
#	exit 1
#  fi
  
}

chaincodeQueryByID () {
  PEER=$1
  TX_ID=$2
  echo "===================== Querying on PEER$PEER on channel '$CHANNEL_NAME'... ===================== "
  setGlobals $PEER
  local rc=1
  local starttime=$(date +%s)

  # continue to poll
  # we either get a successful response, or reach TIMEOUT
  while test "$(($(date +%s)-starttime))" -lt "$TIMEOUT" -a $rc -ne 0
  do
     sleep $DELAY
  	 echo "Attempting to Query PEER$PEER ...$(($(date +%s)-starttime)) secs"
  	 peer chaincode query -C $CHANNEL_NAME -n mycc -c '{"Args":["queryByID", "'$TX_ID'"]}' >&log.txt
#     test $? -eq 0 && VALUE=$(cat log.txt | awk '/Query Result/ {print $NF}')
#     test "$VALUE" = "$2" && let rc=0
  done
  echo
  cat log.txt

#  if test $rc -eq 0 ; then
#	echo "===================== Query on PEER$PEER on channel '$CHANNEL_NAME' is successful ===================== "
#  else
#	echo "!!!!!!!!!!!!!!! Query result on PEER$PEER is INVALID !!!!!!!!!!!!!!!!"
#        echo "================== ERROR !!! FAILED to execute End-2-End Scenario =================="
#	echo
#	exit 1
#  fi
  
}

chaincodeProviderQuery () {
  PEER=$1
  TX_ID=$2
  TOKEN=$3

  echo "===================== Querying on PEER$PEER on channel '$CHANNEL_NAME'... ===================== "
  setGlobals $PEER
  local rc=1
  local starttime=$(date +%s)

  # continue to poll
  # we either get a successful response, or reach TIMEOUT
  while test "$(($(date +%s)-starttime))" -lt "$TIMEOUT" -a $rc -ne 0
  do
     sleep $DELAY
  	 echo "Attempting to Query PEER$PEER ...$(($(date +%s)-starttime)) secs"
  	 peer chaincode query -C $CHANNEL_NAME -n mycc -c '{"Args":["providerQuery", "'$TX_ID'", "'$TOKEN'"]}' >&log.txt
#     test $? -eq 0 && VALUE=$(cat log.txt | awk '/Query Result/ {print $NF}')
#     test "$VALUE" = "$2" && let rc=0
  done
  echo
  cat log.txt

#  if test $rc -eq 0 ; then
#	echo "===================== Query on PEER$PEER on channel '$CHANNEL_NAME' is successful ===================== "
#  else
#	echo "!!!!!!!!!!!!!!! Query result on PEER$PEER is INVALID !!!!!!!!!!!!!!!!"
#        echo "================== ERROR !!! FAILED to execute End-2-End Scenario =================="
#	echo
#	exit 1
#  fi
  
}

chaincodeInsuranceQuery () {
  PEER=$1
  NAME=$2
  TX_ID=$3

  echo "===================== Querying on PEER$PEER on channel '$CHANNEL_NAME'... ===================== "
  setGlobals $PEER
  local rc=1
  local starttime=$(date +%s)

  # continue to poll
  # we either get a successful response, or reach TIMEOUT
  while test "$(($(date +%s)-starttime))" -lt "$TIMEOUT" -a $rc -ne 0
  do
     sleep $DELAY
  	 echo "Attempting to Query PEER$PEER ...$(($(date +%s)-starttime)) secs"
  	 peer chaincode query -C $CHANNEL_NAME -n mycc -c '{"Args":["insuranceQuery", "'$NAME'", "'$TX_ID'"]}' >&log.txt
#     test $? -eq 0 && VALUE=$(cat log.txt | awk '/Query Result/ {print $NF}')
#     test "$VALUE" = "$2" && let rc=0
  done
  echo
  cat log.txt

#  if test $rc -eq 0 ; then
#	echo "===================== Query on PEER$PEER on channel '$CHANNEL_NAME' is successful ===================== "
#  else
#	echo "!!!!!!!!!!!!!!! Query result on PEER$PEER is INVALID !!!!!!!!!!!!!!!!"
#        echo "================== ERROR !!! FAILED to execute End-2-End Scenario =================="
#	echo
#	exit 1
#  fi
  
}

chaincodePatientQuery () {
  PEER=$1
  CASE_ID=$2

  echo "===================== Querying on PEER$PEER on channel '$CHANNEL_NAME'... ===================== "
  setGlobals $PEER
  local rc=1
  local starttime=$(date +%s)

  # continue to poll
  # we either get a successful response, or reach TIMEOUT
  while test "$(($(date +%s)-starttime))" -lt "$TIMEOUT" -a $rc -ne 0
  do
     sleep $DELAY
  	 echo "Attempting to Query PEER$PEER ...$(($(date +%s)-starttime)) secs"
  	 peer chaincode query -C $CHANNEL_NAME -n mycc -c '{"Args":["patientQuery", "'$CASE_ID'"]}' >&log.txt
#     test $? -eq 0 && VALUE=$(cat log.txt | awk '/Query Result/ {print $NF}')
#     test "$VALUE" = "$2" && let rc=0
  done
  echo
  cat log.txt

#  if test $rc -eq 0 ; then
#	echo "===================== Query on PEER$PEER on channel '$CHANNEL_NAME' is successful ===================== "
#  else
#	echo "!!!!!!!!!!!!!!! Query result on PEER$PEER is INVALID !!!!!!!!!!!!!!!!"
#        echo "================== ERROR !!! FAILED to execute End-2-End Scenario =================="
#	echo
#	exit 1
#  fi
  
}

chaincodeInvoke () {
	PEER=$1
	ARG_1=$2
	ARG_2=$3
	setGlobals $PEER
	# while 'peer chaincode' command can get the orderer endpoint from the peer (if join was successful),
	# lets supply it directly as we know it using the "-o" option
	if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
		peer chaincode invoke -o orderer.example.com:7050 -C $CHANNEL_NAME -n mycc -c '{"Args":["visit","'$ARG_1'","'$ARG_2'"]}' >&log.txt
	else
		peer chaincode invoke -o orderer.example.com:7050  --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME -n mycc -c '{"Args":["visit","'$ARG_1'","'$ARG_2'"]}' >&log.txt
	fi
	res=$?
	cat log.txt
	verifyResult $res "Invoke execution on PEER$PEER failed "
	echo "===================== Invoke transaction on PEER$PEER on channel '$CHANNEL_NAME' is successful ===================== "
	echo
}

chaincodeInvokeFuncs () {
	PEER=$1
	FCN=$2
	ARG=$3
	setGlobals $PEER
	# while 'peer chaincode' command can get the orderer endpoint from the peer (if join was successful),
	# lets supply it directly as we know it using the "-o" option
	if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
		peer chaincode invoke -o orderer.example.com:7050 -C $CHANNEL_NAME -n mycc -c '{"Args":["'$FCN'","'$ARG'"]}' >&log.txt
	else
		peer chaincode invoke -o orderer.example.com:7050  --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME -n mycc -c '{"Args":["'$FCN'","'$ARG'"]}' >&log.txt
	fi
	res=$?
	cat log.txt
	verifyResult $res "Invoke execution on PEER$PEER failed "
	echo "===================== Invoke transaction on PEER$PEER on channel '$CHANNEL_NAME' is successful ===================== "
	echo
}

## Create channel
echo "Creating channel..."
sleep 60

createChannel

## Join all the peers to the channel
echo "Having all peers join the channel..."
joinChannel

## Set the anchor peers for each org in the channel
echo "Updating anchor peers for org1..."
updateAnchorPeers 0
echo "Updating anchor peers for org2..."
updateAnchorPeers 2
echo "Updating anchor peers for org3..."
updateAnchorPeers 4
echo "Updating anchor peers for org4..."
updateAnchorPeers 6

## Install chaincode on Peer0/Org1 and Peer2/Org2
echo "Installing chaincode on org1/peer0..."
installChaincode 0

echo "Installing chaincode on org1/peer1..."
installChaincode 1

echo "Installing chaincode on org2/peer0..."
installChaincode 2

echo "Installing chaincode on org2/peer1..."
installChaincode 3

echo "Installing chaincode on org3/peer0..."
installChaincode 4

echo "Installing chaincode on org3/peer1..."
installChaincode 5

echo "Installing chaincode on org4/peer0..."
installChaincode 6

echo "Installing chaincode on org4/peer1..."
installChaincode 7

#Instantiate chaincode on Peer0/Org1
echo "Instantiating chaincode on org1/peer0..."
instantiateChaincode 0

#chaincodeQueryByID 0 T00001

#JSON=$(printf '%s\n' '{\"caseID\":\"C00001\",\"transactionID\":\"T00002\",\"patientID\":\"demo_patient\",\"provider\":{\"providerName\":\"SEAT\",\"code\":\"E11.311\",\"providerAccessToken\":\"PASSWORD\",\"bill\":\"15000\"},\"insurance\":[{\"insuranceName\":\"BCBS\",\"paidAmount\":\"n/a\"},{\"insuranceName\":\"Atena\",\"paidAmount\":\"n/a\"}],\"patient\":{\"PHI\":\"this_is_a_demo_PHI\"}}')

#chaincodeInvokeFuncs 0 providerPost $JSON

#chaincodeQueryByID 0 T00002

#chaincodeProviderQuery 2 T00002 PASSWORD

#chaincodeInsuranceQuery 4 BCBS T00002

#INS=$(printf '%s\n' '{\"caseID\":\"C00001\",\"transactionID\":\"T00003\",\"patientID\":\"demo_patient\",\"provider\":{\"providerName\":\"SEAT\",\"code\":\"E11.311\",\"providerAccessToken\":\"n/a\",\"bill\":\"15000\"},\"insurance\":[{\"insuranceName\":\"BCBS\",\"paidAmount\":\"10000\"},{\"insuranceName\":\"n/a\",\"paidAmount\":\"n/a\"}],\"patient\":{\"PHI\":\"n/a\"}}')

#chaincodeInvokeFuncs 4 insurancePost $INS

#chaincodePatientQuery 6 C00001

echo
echo "========= All GOOD, BYFN execution completed =========== "
echo

echo
echo " _____   _   _   ____   "
echo "| ____| | \ | | |  _ \  "
echo "|  _|   |  \| | | | | | "
echo "| |___  | |\  | | |_| | "
echo "|_____| |_| \_| |____/  "
echo

#exit 0
