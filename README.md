# Building Blockchain Applications with Hyperledger Fabric

Instructors: **Xinchi He** and **Mauricio Papa**

Affiliation: *Tandy School of Computer Science, The University of Tulsa*

Email: **xinchi-he@utulsa.edu**, **mauricio-papa@utulsa.edu**

---

## 1. Blockchain Basics

Blockchain was first introduced by Satoshi Nakamoto as the foundational technology for Bitcoin operations in 2008.
It uses distributed systems to validate, keep and synchronize the contents of a transaction in a replicated immutable ledger.
Currently, blockchain technology has been applied and researched in various domains, such as supply chain and agriculture.

The advantages to adopt blockchain technology in your organization are:

1. Transparency
2. Security
3. Traceability
4. Efficiency
5. Cost

### 1.1 Distributed Immutable Ledgers

The "immutable" property simply means the records that are kept in the ledger are unchangeable.
Immutability is achieved by using cryptography and the blockchain hashing process.

![Blockchain Immutable Ledger](assets/ledger.png)

<img src="assets/basic_network.png" width="600">

### 1.2 Consensus Algorithms

Consensus algorithm is a methodology to make decisions (reach an agreement) within a distributed system.
They are used to ensure the integrity of the blockchain transactions.
The consensus algorithm may be vary depending on the specific blockchain framework being used.
Some blockchain frameworks might also allow to utilize different consensus algorithms in a modular manner. (e.g Hyperlegder)

Here is a list of popular consensus algorithms:

- Proof-of-Work (PoW) 
  - Bitcoin
- Proof-of-Stake (PoS)
  - Ethereum
- Proof-of-Elapsed Time (PoET)
  - Lisk
- Practical Byzantine Fault Tolerance (PBFT)
  - Hyperledger Fabric

<img src="assets/consensus.png" width="600">

### 1.3 Smart Contract

> ***A set of promises, specified in digital form, including protocols within which the parties perform on this promises.***
> --- Sazbos (computer scientist and cryptographer) in the 1990s

Smart contract can be seen as computer protocols that digitally facilitate, verify and enforce the contracts made between two or more parties on blockchain.

Here is a list of popular smart contract implementation:

- Solidity
  - Ethererum Virtual Machine
- Chaincode
  - Golang, Java and Javascript
- SSC (Stellar Smart Contract)
  - Golang, Java and Javascript

<img src="assets/Smart_Contract.png" width="600">

### 1.4 Permissioned vs. Permissionless

Permissionless (public) blockchain allows each party to join the network freely.
In other words, there is no implicit trust built into such model.
Typically, the framework requires a "mining" process to ensure blockchain transaction integrity, such as PoW.

Compared to that of permissionless blockchain, a permissioned (private) blockchain utilizes certificate authorities to authenticate parties when joining the network.
In a permissioned blockchain network, the interactions among different entities are secured, but the entities are not necessarily trusting each other.
Typically, permissioned blockchain uses Crash Fault Tolerance (CFT) or Practical Byzantine Fault Tolerance (PBFT) based consensus algorithms, rather than the costly PoW.


## 2. Introduction to Hyperledger Fabric

### 2.1 Hyperledger Projects (https://hyperledger.org)

Hyperledger is a fairly large project that contains different distributed ledger frameworks, smart contract engines, client libraries and graphical interfaces.

Hyperledger Fabric is an open-source permissioned blockchain framework under Hyperledger project that comes from the Linux Foundation. 
It allows network members to enroll through a trusted **Membership Service Provider (MSP)**.

Here is a list of distributed ledgers that hosted by Hyperledger project:

- Fabric
  - originate from IBM
  - enterprise-grade permissioned distributed ledger framework
  - finance, healthcare, supply chain and etc.
- Burrow
  - smart contract machine
  - interpreter for EVM
- Indy
  - originate from Sovrin Foundation
  - decentralized identity
- Iroha
  - originate from Japan
  - target mobile use cases
  - written in C++
- Sawtooth
  - originate from Intel
  - Proof-of-Elapsed Time (PoET)
  - targets large distributed validator populations with minimal resource consumption

<img src="assets/hyperledger.png" width=700>

### 2.2 Certificate Authorities

In order to participate in the permissioned blockchain network, different roles are required to authenticate with security gateways.
The security gateways in Hyperledger Fabric are called certificate authorities (CA).
The digital identify of each role is encapsulated in an X.509 digital certificate.

<img src="assets/certificate_authorities.png">

### 2.3 Membership

Membership Service Provider (MSP) allows us to define members of a trust domain (e.g. organization).

<img src="assets/membership.diagram.3.png" width="700">

### 2.4 Peers

Peers are physical nodes that participate in the blockchain network.
Peer node holds the smart contract(s) and the distributed shared immutable ledger(s).

- One peer node can hold more than one smart contract
- One peer node can hold more than one ledger (involve in multiple channels)

#### 2.4.1 Peers and Applications

<img src="assets/peers.diagram.6.png" width=700>

#### 2.4.2 Peers and Channels

<img src="assets/peers.diagram.5.png" width=700>

#### 2.4.3 Peers and Organizations

<img src="assets/peers.diagram.8.png" width=700>

### 2.5 Ledger

In Hyperledger Fabric, the ledger has two components:
- World State
  - A database that holds a cache of the current values of a set of ledger states
- Blockchain
  - A transaction log that records all the changes that have resulted in the current world state

<img src="assets/ledger.diagram.1.png" width=700>

<img src="assets/ledger.diagram.3.png" width=700>

### 2.6 Chaincode

Chaincode is the smart contract implementation in Hyperledger Fabric.
Currently, Chincode supports three different types of programming languages:
1. Golang (<- we are going to focus on this one for the workshop)
2. Java
3. Javascript

There are mainly three operations to the world states in the ledger:
1. ***get***
2. ***put***
3. ***delete***

### 2.7 Ordering Service 

Ordering can be seen as the consensus process in Hyperledger Fabric.
Because Hyperledger Fabric relies on deterministic consensus algorithms, ordering service is used to establish total order of the transactions.
Ordering service collects transactions from the client, then order and group them into blocks one after another.

Orderers and the transaction flow:
1. Proposal
2. Ordering and packaging transactions into blocks
3. Validation and commit

Ordering service implementations:
- Solo (for development only)
- Kafka (since 1.0)
- Raft (since 1.4, very new)
- BFT (work in progress)

## 3. Build your own blockchain network

Before starting our blockchain application journey, we need to do a little bit preparation.
The tutorial is based on Debian Linux, and your mileage may vary.

### 3.1 Install Necessary Dependencies

```bash
$ sudo apt install build-essential golang docker docker-compose
```

### 3.2 Clone *fabric-sample* Repository

This is a official github repository for Hyperledger Fabric examples.
We will start from here.

```bash
$ cd ~ && git clone https://github.com/hyperledger/fabric-samples
```

### 3.3 Install Binaries and Docker Images

*Note: The tutorial is based on Hyperledger Fabric 1.1.x, which is a stable release.*
*If you are interested in newer production releases, please see the official documentation for further information.*

```bash
$ git checkout release-1.1
$ curl -sSL https://goo.gl/6wtTN5 | bash -s 1.1.1 1.1.1 0.4.6
```

Downloading docker images might take a while.
After fetching the images, make sure you have the images lists similar to the one below:

```console
$ docker images
REPOSITORY                     TAG                 IMAGE ID            CREATED             SIZE
hyperledger/fabric-zookeeper   latest              20c6045930c8        6 months ago        1.43GB
hyperledger/fabric-kafka       latest              b4ab82bbaf2f        6 months ago        1.44GB
hyperledger/fabric-ca          latest              1a804ab74f58        8 months ago        244MB
hyperledger/fabric-tools       latest              b7bfddf508bc        18 months ago       1.46GB
hyperledger/fabric-tools       x86_64-1.1.0        b7bfddf508bc        18 months ago       1.46GB
hyperledger/fabric-orderer     latest              ce0c810df36a        18 months ago       180MB
hyperledger/fabric-orderer     x86_64-1.1.0        ce0c810df36a        18 months ago       180MB
hyperledger/fabric-peer        latest              b023f9be0771        18 months ago       187MB
hyperledger/fabric-peer        x86_64-1.1.0        b023f9be0771        18 months ago       187MB
hyperledger/fabric-javaenv     latest              82098abb1a17        18 months ago       1.52GB
hyperledger/fabric-javaenv     x86_64-1.1.0        82098abb1a17        18 months ago       1.52GB
hyperledger/fabric-ccenv       latest              c8b4909d8d46        18 months ago       1.39GB
hyperledger/fabric-ccenv       x86_64-1.1.0        c8b4909d8d46        18 months ago       1.39GB
hyperledger/fabric-baseos      x86_64-0.4.6        220e5cf3fb7f        19 months ago       151MB
```

### 3.4 Run *first-network*

*first-network* is an official example on setting up a blockchain network with 2 organizations and 2 peers for each organization.
Let's run this example and see what a Hyperledger Fabric network looks like.

```bash
$ cd fabric-samples/first-network
$ ./byfn.sh -m generate # generate certificates and genesis block
$ ./byfn.sh -m up # start the network
```

There are two types of configuration files for the *first-network* example:

1. ```scripts/{script.sh, utils.sh}``` to setup peer nodes and channels
2. ```base/docker-compose-base.yaml``` to define the architecture of the blockchain network

### 3.5 Practice

Based on the *first-network* example, expand the network architecture to 4 organizations and 2 peers for each organization.

## 4. Smart Contract Development and Deployment

In Hyperledger Fabric, the smart contract implementation is called ***chaincode***.
In this tutorial, we are going to use Golang-based chaincode.

### 4.1 Chaincode Development

The chaincode is basically pure Go code that is organized with the following structure:

<img src="assets/chaincode.png" width=700>

Some of the Hyperledger Fabric libraries are used in the chaincode development, for example:

- [```shim```](https://godoc.org/github.com/hyperledger/fabric/core/chaincode/shim) provides APIs to access state variables, transaction context and calling other chaincodes.
- [```peer```](https://godoc.org/google.golang.org/grpc/peer) defines blockchain peer node information associated with RPCs and utilities.

Let's take a look at the chaincode that deployed in the *first-network* example, the file is located at ```chaincode/chaincode_example02/go/chaincode_example02.go```

### 4.2 Chaincode Deployment

The blockchain network won't load the newly deployment chaincode unless the network administrator install and instantiate the chaincode.
Chaincode can be deployed through:
1. Command line
2. RESTful API

In this section, we will focus on the command line methodology for chaincode deployment.
Recall that in the *first-network* example, we have ```scripts/script.sh``` shell script.
Find the following lines in such shell script:

```bash
CC_SRC_PATH="github.com/chaincode/chaincode_example02/go/"
if [ "$LANGUAGE" = "node" ]; then
	CC_SRC_PATH="/opt/gopath/src/github.com/chaincode/chaincode_example02/node/"
fi
```

We can specify the chaincode path with ```CC_SRC_PATH``` variable.

Then in ```scripts/utils.sh```, the ```installChaincode()``` function contains the actual command to load chaincode into corresponding peer nodes.

```bash
installChaincode () {
	PEER=$1
	ORG=$2
	setGlobals $PEER $ORG
	VERSION=${3:-1.0}
        set -x
	peer chaincode install -n mycc -v ${VERSION} -l ${LANGUAGE} -p ${CC_SRC_PATH} >&log.txt
	res=$?
        set +x
	cat log.txt
	verifyResult $res "Chaincode installation on peer${PEER}.org${ORG} has Failed"
	echo "===================== Chaincode is installed on peer${PEER}.org${ORG} ===================== "
	echo
}
```

### 4.3 Practice

Based on the *chaincode_example02* chaincode that used in *first-network*, create a new chaincode that have four variables (A, B, C, D) defined.
And then the ```invoke()``` function should be able to specify values for each individual variable.

e.g: ```invoke("A", 20)``` updates the state of variable A to 20.

Deploy the new chaincode into the *first-network* blockchain network.


## 5. Integration with RESTful APIs

We have our blockchain network running from the previous steps, but what if we need to integrate our blockchain application with external services?
Thanks to Hyperledger Fabric Client, which is a Nodejs based SDK, we are able to query, invoke, install and instantiate chaincodes in a RESTful manner.

As the Hyperledger Fabric Client SDK when are going to use today is based on Nodejs 8.x, please make sure the Nodejs version installed in your environment is not newer than 8.x.

### 5.1 Install Nodejs 8.x

```bash
$ curl -sL https://deb.nodesource.com/setup_8.x | sudo bash -
$ sudo apt install nodejs
$ sudo apt install -y --allow-downgrades nodejs=8.16.1-1nodesource1
```

### 5.2 Install jq

*jq* is a lightweight and flexible JSON processor, it is used in this tutorial to help parse the JSON-based HTTP response from the RESTful API.

```bash
$ sudo apt install jq
```

Now go to ```fabric-samples/balance-transfer``` folder, this is a sample project utilizing Fabric Client SDK to create channel, install chaincode and manage blockchain networks.
It is worth mentioning that such sample projects has the following network architecture:
- 2 CA
- 2 organizations (2 peers each)
- Solo ordering

The SDK communicates with the blockchain network using gRPC protocol and wraps the interfaces with RESTful APIs.

### 5.3 Start the blockchain network

```bash
$ cd ~/fabric-samples/balance-transfer
$ ./runApps.sh
```

*Optional: ```runApps.sh``` script will install the node modules that defined in ```package.json``` automatically dy default. If errors occur during the modules installation, run ```npm rebuild``` later.*  

After the blockchain network is successfully setting up, let's test different functionality for RESTful APIs

Open a separate terminal window

```bash
$ cd ~/fabric-samples/balance-transfer
$ ./testAPIs.sh
```

*Optional: If you encounter some issues during the "join-channel" process, replace lines 155-157 in ```app/join-channel.js``` with the code below and add ```sleep 5``` in ```testAPIs.sh``` before the chaincode instantiation. This is because the nodejs runtime in your environment might not be compatible with the codes.*

```javascript
try {
        let message = util.format('Failed to join all peers to channel. cause:%s',error_message);
        logger.error(message);

} catch {

}

//throw new Error(message);
```

Now let's look at contents in ```testAPIs.sh``` in details, the shell script contains the RESTful call with the functionality below:

- Enroll users
- Create a new channel
- Join channel for peers (two organizations, four peers)
- Install chaincode on all peers
- Instantiate chaincode
- Invoke chaincode (i.e. transfer money from b to a)
- Query chaincode (i.e. check the balance of a)
- Query transaction using a TransactionID
- Query general info, like chain info, installed chaincodes, instantiated chaincodes, channel

## 6. Ready to take off? An end-to-end application: OTA Firmware Update Verification for IoT devices

In previous sections, we have learned the basics about the Hyperledger Fabric, let recall again
- Deploy your first blockchain network, customize blockchain network architecture
- Chaincode development and deployment
- Interacting with the blockchain network - RESTful APIs

Now, let's do something more practical - use Hyperledger Fabric to secure the OTA firmware updates for IoT devices.

### 6.1 Why?

As mentioned earlier, blockchain technology can be used for security enhancement, because the distributed nature of blockchain avoids single point of failure and resilient to cyber-attacks.

**Challenges:** Current OTA firmware update solutions favor client and server model, which is a centralized architecture. 
In addition, the nature of IoT devices (e.g. limited hardware resources) makes it harder to directly adopt traditional security mechanisms.

### 6.2 Solution 

Use smart contract to verify the legitimacy of firmware updates
- Store firmware update information in the ledger
- Use SHA1 based firmware hash for verification

**Advantages**

- Keep track of all events associated with the firmware update
- Manufacturers can use smart contract to specify update conditions in a flexible manner
- Resilient to network failures and cyber-attacks

**Overall Architecture**

<img src="assets/Implementation_Overview_(Colored).png" width=400>

**Blockchain Network Architecture**

<img src="assets/Blockchain_Network.png" width=400>

### 6.3 Clone Repository and Prepare Environments

```bash
$ git clone https://gitlab.com/azur3s0ng/hlf_workshop #clone the repo
$ cp -r ~/hlf_workshop/iot-firmware-kafka ~/fabric-samples/ #move project to the base directory
$ cp -r ~/hlf_workshop/chaincode/iot ~/fabric-samples/chaincode #move chaincode to the base directory 
$ cd ~/fabric-samples/iot-firmware-kafka && npm install #install node modules
```

### 6.4 Start the Blockchain Network

```bash
$ ./clean.sh #clean the docker environment
$ ./restart.sh #start blockchain network
```

### 6.5 Start the Fabric Client (RESTful API)

```bash
$ PORT=4000 node app
```

If everything goes well, you should see something similar to this:
```console
[2019-09-18 20:17:48.556] [INFO] SampleWebApp - *************** SERVER STARTED ***************
[2019-09-18 20:17:48.556] [INFO] SampleWebApp - ************ http://localhost:4000 ************
```

### 6.6 Flash Base Firmware to Wemos D1 Mini

In this section, we need to download the OTA firmware update libraries to the Wemos D1 Mini chip.

1. Arduino IDE (Windows, Mac OS X and Linux): https://wiki.wemos.cc/downloads
2. CH340G Driver (USB Serial, only needed for Windows and Mac OS X): https://wiki.wemos.cc/downloads
3. Install Wemos-board support in Arduino IDE:
   - Start Arduino IDE and open Preferences window.
   - Enter: http://arduino.esp8266.com/versions/2.3.0/package_esp8266com_index.json into the Additional Boards Manager URLs field.
   - Open Tools->Board:xx->Boards Manager and install esp8266 by ESP8266 Community
   - Open the Tools-> Board menu after installation) and select LOLIN(WEMOS) D1 R2 & mini as the board to use.
4. Install the following library (ArduinoJson 5.13.3 <- Please use this exact version) 
5. Open firmware v1 project (File -> Open -> hlf_workshop/esp8266/v1/v1.ino)
6. Replace the following lines with wireless network credentials
   ```c
   const char* ssid = "SSID";
   const char* password = "PASSWORD";
   ```
7. Edit ```hlf_workshop/esp8266/v1/src/ESP8266HTTPUpdateServer_mod.cpp```, replace the IP address in lines 136, 153 and 174 with yours and save the file. 
8. Select the IoT chip under Tools -> Port
9. Upload

If everything goes well, you should see similar information as the follows from the console output:
```
Sketch uses 307024 bytes (29%) of program storage space. Maximum is 1044464 bytes.
Global variables use 31064 bytes (37%) of dynamic memory, leaving 50856 bytes for local variables. Maximum is 81920 bytes.
warning: serialport_set_baudrate: baud rate 921600 may not work
Uploading 311168 bytes from /var/folders/zy/xwnbcz397qj33zys8n7_0jjh0000gn/T/arduino_build_267771/v1.ino.bin to flash at 0x00000000
................................................................................ [ 26% ]
................................................................................ [ 52% ]
................................................................................ [ 78% ]
................................................................                 [ 100% ]
```

Open serial monitor (Tools -> Serial Monitor) and press reset button on the chip.

```
Booting Sketch...
HTTPUpdateServer ready! Open http://192.168.1.101/update in your browser
firmware v1.0
```

<!--
### 6.7 Setup Vendor Service

The vendor service is a simple web service written in Golang, it is used to deliver new firmware update request.

Open ```fabric-samples/iot-firmware-kafka/vendor_service/vendor_service.go``` in text editor.
- Replace lines 37, 48, 61, 72 with the your host IP address.
- Replace line 55 with your Wemos D1 Mini IP address (can be obtained from the serial monitor)

```bash
$ cd ~/fabric-samples/iot-firmware-kafka/vendor_service
$ go run vendor_service.go
```
-->

### 6.7 Shell Script to Push New Firmware Update Request 

In ```fabric-samples/iot-firmware-kafka```, there should be a shell script ```push.sh```.
Such script is used to push new firmware update request to the blockchain network.

In Arduino IDE, open ```hlf_workshop/esp8266/v2/v2.ino``` and set the wireless network credentials as well. Save the binary and obtain the SHA1 hash of the binary (http://onlinemd5.com/)

Then replace the obtained SHA1 hash of v2 in the ```push.sh``` script.

```bash
transactionID=$(echo ESP00001)
timestamp=$(echo "2018-11-30_16:40:00")
hashedMSG=$(echo "457de643c3113667f18660bf12c999db721a3fc4") #<----Replace the SHA1 of v2 here
deviceType=$(echo "ESP8266")
version=$(echo "2.0")

echo
TRX_ID=$(curl -s -X POST \
http://localhost:4000/channels/mychannel/chaincodes/mycc \
-H "authorization: Bearer $ORG1_TOKEN" \
-H "content-type: application/json" \
-d '{
"fcn":"push",
"args":["'$transactionID'","'$timestamp'","'$hashedMSG'","'$deviceType'","'$version'"]
}')
echo "Transacton ID is $TRX_ID"
echo
```

### 6.8 Firmware Update Verification Workflow

1. Open a new terminal and run ```./push.sh```
   1. *Optional: if you encounter a invoke-chaincode timeout error from the node client, simply increase the timeout value in ```app/invoke-transactions.js```*
2. Open a separate tab, and input URL ```http://YOUR_WEMOS_IP/update``` and then select the update binary in ```v2``` folder

*Note: due to a firmware update protection mechanism that enforced in the smart contract, you only have 60 seconds in between step 1 and 2. If you see a transaction expire error, simply repeat step 1 again to reset the expiration clock*

If everything goes well, you should see the following similar outputs from the Arduino serial monitor (v1->v2):

```
final hash is 457de643c3113667f18660bf12c999db721a3fc4
Verifying with blockchain...
200
{"success":true,"secret":"MyTypQcizPrS","message":"ESP8266 enrolled Successfully","token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Njg5NjYzNjQsInVzZXJuYW1lIjoiRVNQODI2NiIsIm9yZ05hbWUiOiJvcmcxIiwiaWF0IjoxNTY4OTMwMzY0fQ.U_oj3CxqUn32N-EZd8hZWwwEZ9G_WZNVx_ktrfKW9mM"}
Hyperledger Client Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Njg5NjYzNjQsInVzZXJuYW1lIjoiRVNQODI2NiIsIm9yZ05hbWUiOiJvcmcxIiwiaWF0IjoxNTY4OTMwMzY0fQ.U_oj3CxqUn32N-EZd8hZWwwEZ9G_WZNVx_ktrfKW9mM
{"fcn": "verify", "args": ["ESP00001", "457de643c3113667f18660bf12c999db721a3fc4"]}
200
Transaction ID: 8bc264313722427ed339e5ada58600cb3ab95cef50b45f9317f94935612c3919
{"status":"verified","failed_attemps":0}
Verification Succeed! Firmware is legtimate, will reboot to finalize the OTA update.
Update Success: 297104
Rebooting...

 ets Jan  8 2013,rst cause:2, boot mode:(3,6)

load 0x4010f000, len 1384, room 16 
tail 8
chksum 0x2d
csum 0x2d
vbb28d4a3
@cp:0
ld

Booting Sketch...
HTTPUpdateServer ready! Open http://esp8266-webupdate.local/update in your browser
firmware v2.0
```

### 6.9 Practice

Simply alter the v2 Arduino code by injecting random lines of code (e.g. print hello world) and save the binary.
Redo the firmware update process and observe what will be shown in the serial monitor.

## 7. Useful Resources
- [Hyperledger Official Rocket Chat Channel](https://chat.hyperledger.org/channel/fabric) 
  - In case you have any related questions to ask
- [Hyperledger Fabric Paper](https://arxiv.org/pdf/1801.10228.pdf)
  - If you are interested in reading the original paper of Hyperledger Fabric 
- [Hyperledger Fabric Documentation](https://hyperledger-fabric.readthedocs.io/en/release-1.4/)
  - Your good friend
- [Securing Over-The-Air IoT Firmware Updates using Blockchain](https://www.researchgate.net/publication/332278969_Securing_Over-The-Air_IoT_Firmware_Updates_using_Blockchain)
  - If you are interested in the theory behind Section 6
  - Also, we will have a presentation session tomorrow

